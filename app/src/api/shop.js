const _products = [
    {"id": 1, "producer": "Must have", "title": "Pinkman", "flavors": [1, 2, 3], "strengthRate": 3, "flavorRate": 4, "description": "Good tobacco!"},
    {"id": 2, "producer": "Must have", "title": "Lemograss", "flavors": [1, 2, 3], "strengthRate": 3, "flavorRate": 4, "description": "Good tobacco!"},
    {"id": 3, "producer": "Must have", "title": "Pinkman", "flavors": [1, 2, 3], "strengthRate": 3, "flavorRate": 4, "description": "Good tobacco!"},
];

const _flavors = [
    {"id": 1, "title": "Дыня", "color": "#f4A460"},
    {"id": 2, "title": "Арбуз", "color": "#fc6c85"},
    {"id": 3, "title": "Манго", "color": "#ffb960"}
];

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default {
    getProducts(cb) {
        setTimeout(() => cb(_products), 100);
    },

    async getFlavors() {
        await delay(1000);
        return _flavors;
    }
}
