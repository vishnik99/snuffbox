import Vue from 'vue';
import Vuex from 'vuex';
import products from "@/store/modules/products";
import flavors from "@/store/modules/flavors";

Vue.use(Vuex);

export default new Vuex.Store({
   modules: {
      products,
      flavors,
   },
   strict: process.env.NODE_ENV !== 'production',
});