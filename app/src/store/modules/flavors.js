import shop from '../../api/shop';

const state = () => ({
    all: [],
});

const getters = {
    flavors: state => {
        return state.all;
    },
    get_flavor: state => id => state.all.find(flavor => flavor.id === id)
};

const actions = {
    GET_FLAVORS: async (context) => {
        const flavors = await shop.getFlavors();
        await context.commit('SET_FLAVORS', flavors);
    },
}


const mutations = {
    SET_FLAVORS: (state, flavors) => {
        state.all = flavors;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
