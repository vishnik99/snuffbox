import Vue from 'vue'
import Router from 'vue-router'

import PutSnuffPage from "@/components/PutSnuffPage";
import SnuffListPage from "@/components/SnuffListPage";
Vue.use(Router)

export default new Router({
    mode: 'history',
    hashbang: false,
    base: process.env.BASE_URL,
    root: '/',
    routes: [
        {
            path: '/putSnuff',
            name: 'PutSnuffPage',
            component: PutSnuffPage
        },
        {
            path: '/snuffList',
            name: 'SnuffListPage',
            component: SnuffListPage
        }
    ]
})